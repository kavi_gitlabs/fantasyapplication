package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class ContestService {
	
  
	@Autowired
	private ContestRepository ContestRepo;
	
	
	public Contest createContest(Contest contest) {
		return ContestRepo.save(contest);
	}
	

//	GET contest
	public List<Contest> getContests() {
		return ContestRepo.findAll();
	}
	
	

//	GET fixture by ID
	public Contest getContestbyId(int id) {
		
		return ContestRepo.findById(id).orElse(null);
	}
	
	

//	Edit fixture by ID
	public Contest editcontest(@RequestBody Contest contest) {

		
		Optional<Contest> currentfixture = ContestRepo.findById(contest.getId());
		
		currentfixture.get().setContestname(contest.getContestname());
		
		currentfixture.get().setMaxsize(contest.getMaxsize());
		currentfixture.get().setMinsize(contest.getMinsize());
		currentfixture.get().setPrizemoney(contest.getPrizemoney());
		currentfixture.get().setEntryfee(contest.getEntryfee());
		currentfixture.get().setRank1(contest.getRank1());
		currentfixture.get().setRank2(contest.getRank2());
		currentfixture.get().setRank3(contest.getRank3());
		
		return ContestRepo.save(currentfixture.get());
		
		
	}
	
	

	
	//Delete by Id 
	
	public String deleteContestbyId(int id) {
		ContestRepo.deleteById(id);
		return "Contest removed " + id ;
	}
	
	
	
}
