package com.example.demo;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;


@Entity
@Table  (name = "team_table")

public class Team {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;  
	
	
	@Column(name="name")
	private String name;
	
	
	@Column(name="logo")
	private String logo;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLogo() {
		return logo;
	}


	public void setLogo(String logo) {
		this.logo = logo;
	}


	
	
	
	
	


}
