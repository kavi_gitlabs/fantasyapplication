package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;


@Service
public class TeamService   {
	
	@Autowired
	private TeamRepository teamRep;
	
 
//	 POST new Team (OK)
	public Team createTeam(Team team) {
		
		System.out.println(team);
		return teamRep.save(team);
	}
	
	
//	GET teams
	public List<Team> getTeams() {
		return teamRep.findAll();
	}
	
	
	
//	GET team by ID
	public Team getTeambyId(int id) {
		
		return teamRep.findById(id).orElse(null);
	}
	
	
	
//	GET team by Name
//	public Team getTeambyName(String name) {
//		return teamRep.findByName(name);
//	}
	
	
	
//	DELETE team by ID
	public String deleteTeambyId(int id) {
		teamRep.deleteById(id);
		return "Team removed " +id ;
	}
	
	
	
//	Edit team by ID
	public Team editTeam(@RequestBody Team team) {

		
		Optional<Team> currentTeam = teamRep.findById(team.getId());
		
		currentTeam.get().setName(team.getName());
		currentTeam.get().setLogo(team.getLogo());
		return teamRep.save(currentTeam.get());
	}

	
}















