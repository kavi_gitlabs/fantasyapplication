package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;






@CrossOrigin(origins="*")
@RestController
@RequestMapping("/")
public class UserMappedController {
	
	
	@Autowired
	private userMappedRepository UMRepo ;
	
	@PostMapping("/createMappedUserContest")
	public UserContestData createUserMappedContest(@RequestBody UserContestData userContestData) {
		
		
		UMRepo.save(userContestData);
		
		return userContestData; 
		
	}
	
	
	@GetMapping("/mappedUserContest")
	public List<UserContestData> getUserMappedContest() {
		
		return UMRepo.findAll();
		
		
	}
	
	}

