package com.example.demo;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface userMappedRepository extends MongoRepository<UserContestData,String>  {

}
