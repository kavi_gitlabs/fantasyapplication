
package com.example.demo;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins="*")
@RestController
@RequestMapping("/")

public class PlayerController {
	

	@Autowired
	PlayerService ps;
	
	@GetMapping("/")
	public String Firstapp() {
		return "R u looking 4 me";
	}
	
//	 POST new Team
	
	@PostMapping("/addPlayer")
	public Player createPlayer(@RequestBody Player player) {
		
		return ps.createPlayer(player);	
		
	}

	@GetMapping("/players")
	public List<Player> findAllPlayers( ) {
		return ps.getPlayers();
}
	
	
//	GET team by ID
	@GetMapping("/players/{id}")
	public Player findPlayerbyId(@PathVariable int id) {
		
		return ps.getplayerbyId(id);	
}
	
	
	
//	Edit team by ID
	@PutMapping("/editplayer")
	public Player editPlayer (@RequestBody Player player) {
		
		 return ps.editPlayer(player);
		 
	}

//	Delete team by ID
@DeleteMapping("deleteplayer/{id}")
public void deletePlayer (@PathVariable int id) {
	
	  ps.deletePlayerbyId(id);
	 
}


}
