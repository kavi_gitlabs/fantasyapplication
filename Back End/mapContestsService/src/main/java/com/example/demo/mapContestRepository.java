package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface mapContestRepository  extends JpaRepository<mapContest, Integer>{

	
	
	@Query(value = "SELECT * FROM mapcontest WHERE matchid = :customParam", nativeQuery = true)	
	    List<mapContest> findByName(@Param("customParam") int customParam);

	
	
	
	
}
 