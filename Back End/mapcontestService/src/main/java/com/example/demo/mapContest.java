package com.example.demo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table  (name = "mapcontest")


public class mapContest {
	
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; 


	@Column(name="contestname")
	private String contestname;
	
	
	@Column(name="contestid")
	private Integer contestid;

	@Column(name="team1") 
	private String team1;
	
	@Column(name="team2")
	private String team2;
	
	@Column(name="matchid")
	private Integer matchid;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContestname() {
		return contestname;
	}

	public void setContestname(String contestname) {
		this.contestname = contestname;
	}

	public Integer getContestid() {
		return contestid;
	}

	public void setContestid(Integer contestid) {
		this.contestid = contestid;
	}

	public String getTeam1() {
		return team1;
	}

	public void setTeam1(String team1) {
		this.team1 = team1;
	}

	public String getTeam2() {
		return team2;
	}

	public void setTeam2(String team2) {
		this.team2 = team2;
	}

	public Integer getMatchid() {
		return matchid;
	}

	public void setMatchid(Integer matchid) {
		this.matchid = matchid;
	}
	
	

	
	
	
	
	
	
	
}
