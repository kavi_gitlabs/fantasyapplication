package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;


@Service
public class FixtureService { 

	
	@Autowired
	private FixtureRepository FixRepo;
	
//	 POST new fixture (OK)
	public Fixture createFixture(Fixture fixture) {
		
		return FixRepo.save(fixture);
	}
	

//	GET fixture
	public List<Fixture> getFixtures() {
		return FixRepo.findAll();
	}
	
	

//	GET fixture by ID
	public Fixture getfixturebyId(int id) {
		
		return FixRepo.findById(id).orElse(null);
	}
	
	

//	Edit fixture by ID
	public Fixture editfixture(@RequestBody Fixture fixture) {

		
		Optional<Fixture> currentfixture = FixRepo.findById(fixture.getId());
		
		currentfixture.get().setTeam1(fixture.getTeam1());
		
		currentfixture.get().setTeam2(fixture.getTeam2());
		currentfixture.get().setDatetime(fixture.getDatetime());
		currentfixture.get().setVenue(fixture.getVenue());
		currentfixture.get().setStage(fixture.getStage());
		
		return FixRepo.save(currentfixture.get());
	}
	
	
	//Delete by Id 
	
	public String deleteFixturebyId(int id) {
		FixRepo.deleteById(id);
		return "Fixture removed " + id ;
	}
	
	
}
