import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FixtureService } from "../fixture.service";
import { MapserviceService } from "../mapservice.service";
import { SharedService } from "../shared.service";
import { UserService } from "../user.service";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.scss"],
})
export class UserComponent implements OnInit {
  finalCurrentUser: any;
  baPoSix: any;
  boPoDot: any;
  fiPoBoundary: any;
  fiPoSix: any; 
  fiPoCatch: any;
  oTPoCap: any;
  oTPoWK: any;
  appPropName: any;
  constructor(
    public userService: UserService,
    private fixService: FixtureService,
    private teamService: SharedService,
    private router: Router
  ) {}
  allfixtures: any;
  allteams: any;
  pointsdata: any;

  loginStatus: boolean = false;

  pointsDataFinal = [];

  baPoRuns: any;
  baPoBoundary: any;
  boPoWicket: any;
  fiPoRuns: any;
  oTPoRuns: any;


  ngOnInit(): void {
    this.finalCurrentUser = this.userService.currentUser;
    this.viewFixtures();
    this.viewTeams();
    this.viewPoints();


    // if (this.finalCurrentUser === undefined) {
    //   this.loginStatus = true;
    // } else {
    //   this.loginStatus = true;
    // }
  }

  viewFixtures() {
    this.fixService.getAllFixtures().subscribe({
      next: (res) => {
        this.allfixtures = res;
        // console.log(this.allfixtures);
      },
    });
  }

  logout() {
    // confirm()
    // this.loginStatus = false;
    // this.finalCurrentUser = undefined;
  }
  login() {
    this.router.navigateByUrl("login");
  }
   
  rules() {
    this.router.navigateByUrl("login/user/userrules");
  }

  viewPoints() {

    this.userService.getPointsData().subscribe({



      next: (res) => {



        this.pointsdata = res;
        // baPoRuns + '-'+ baPoBoundary + '-'+boPoWicket+ '-'+ fiPoRuns + '-'+ oTPoRuns;
        this.pointsDataFinal = this.pointsdata.split("-");

        this.baPoRuns = this.pointsDataFinal[0];
        this.userService.baPoRuns = this.pointsDataFinal[0];
        this.baPoBoundary = this.pointsDataFinal[1];
        this.userService.baPoBoundary = this.pointsDataFinal[1];
        this.baPoSix = this.pointsDataFinal[2];
        this.userService.baPoSix = this.pointsDataFinal[2];
        this.boPoWicket = this.pointsDataFinal[3];
        this.userService.boPoWicket = this.pointsDataFinal[3];
        this.boPoDot = this.pointsDataFinal[4];
        this.userService.boPoDot = this.pointsDataFinal[4];
        this.fiPoBoundary = this.pointsDataFinal[5];
        this.userService.fiPoBoundary = this.pointsDataFinal[5];
        this.fiPoSix = this.pointsDataFinal[6];
        this.userService.fiPoSix = this.pointsDataFinal[6];
        this.fiPoCatch = this.pointsDataFinal[7];
        this.userService.fiPoCatch = this.pointsDataFinal[7];
        this.oTPoCap = this.pointsDataFinal[8];
        this.userService.oTPoCap = this.pointsDataFinal[8];
        this.oTPoWK = this.pointsDataFinal[9];
        this.userService.oTPoWK = this.pointsDataFinal[9];
        this.appPropName = this.pointsDataFinal[10];
        this.userService.appPropName = this.pointsDataFinal[10];


        this.userService.rule1 = this.pointsDataFinal[11];
        this.userService.rule2 = this.pointsDataFinal[12];
        this.userService.rule3 = this.pointsDataFinal[13];
        this.userService.rule4 = this.pointsDataFinal[14];
      },
    });
  }

  viewTeams() {
    this.teamService.getAllTeams().subscribe({
      next: (res) => {
        this.allteams = res;
        // console.log(this.allteams);
      },
    });
  }
}
