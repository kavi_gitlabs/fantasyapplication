import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminLoginComponent } from "./admin-login/admin-login.component";
import { AdminComponent } from "./admin/admin.component";
import { AllTeamsComponent } from "./admin/all-teams/all-teams.component";
import { AllusersComponent } from "./admin/allusers/allusers.component";
import { ChangepasswordComponent } from "./admin/changepassword/changepassword.component";
import { ContestComponent } from "./admin/contest/contest.component";
import { CreatecontestComponent } from "./admin/createcontest/createcontest.component";
import { CreatenewteamComponent } from "./admin/createnewteam/createnewteam.component";
import { FixturesComponent } from "./admin/fixtures/fixtures.component";
import { LivematchComponent } from "./admin/livematch/livematch.component";
import { MapComponent } from "./admin/map/map.component";
import { NewfixturesComponent } from "./admin/newfixtures/newfixtures.component";
import { NewplayerComponent } from "./admin/newplayer/newplayer.component";
import { PlayerComponent } from "./admin/player/player.component";
import { ResultComponent } from "./admin/result/result.component";
import { RulesComponent } from "./admin/rules/rules.component";
import { UsercontestsComponent } from "./admin/usercontests/usercontests.component";
import { UsermappedcontestsComponent } from "./admin/usermappedcontests/usermappedcontests.component";
import { UsermatchesComponent } from "./admin/usermatches/usermatches.component";
import { AllrounderComponent } from "./admin/userplayers/allrounder/allrounder.component";
import { BatterComponent } from "./admin/userplayers/batter/batter.component";
import { BowlerComponent } from "./admin/userplayers/bowler/bowler.component";
import { UserplayersComponent } from "./admin/userplayers/userplayers.component";
import { WkeeperComponent } from "./admin/userplayers/wkeeper/wkeeper.component";
import { UserrulesComponent } from "./admin/userrules/userrules.component";
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./login/login.component";
import { MatchesComponent } from "./matches/matches.component";
import { SignupComponent } from "./signup/signup.component";
import { TempDropComponent } from "./temp-drop/temp-drop.component";
import { UserComponent } from "./user/user.component";

const routes: Routes = [
  { path: "", component: SignupComponent },
  { path: "matches", component: MatchesComponent },
  { path: "home", component: HomeComponent },
  { path: "signup", component: SignupComponent },
  { path: "login", component: LoginComponent },
  {
    path: "login/user",
    component: UserComponent, 
    children: [
      { path: "", component: UsermatchesComponent },
      { path: "usermatches", component: UsermatchesComponent },
      { path: "userrules", component: UserrulesComponent },
      { path: "usercontests", component: UsercontestsComponent },
      {
        path: "userplayers",
        component: UserplayersComponent, 
        children: [
          { path: "", component: WkeeperComponent },
          { path: "wk", component: WkeeperComponent },
          { path: "batter", component: BatterComponent },
          { path: "bowler", component: BowlerComponent },
          { path: "allrounder", component: AllrounderComponent }
        ],
      },
    ],
  },
  { path: "usermatch", component: UserComponent },
  { path: "adminlogin", component: AdminLoginComponent },
  { path: "admin/createNew", component: CreatenewteamComponent },
  { path: "admin/allusers", component: AllusersComponent },
  { path: "admin/createContest", component: CreatecontestComponent },
  { path: "admin/userMappedContest", component: UsermappedcontestsComponent },
  { path: "admin/contest", component: ContestComponent },
  { path: "admin/allteams", component: AllTeamsComponent },
  { path: "admin/rules", component: RulesComponent },
  { path: "admin/changeadminpassword", component: ChangepasswordComponent },
  { path: "admin/livematches", component: LivematchComponent },
  { path: "admin/map", component: MapComponent },
  { path: "admin/fixtures", component: FixturesComponent },
  { path: "admin/result", component: ResultComponent },
  { path: "admin/players", component: PlayerComponent },
  { path: "admin/password", component: ChangepasswordComponent },
  { path: "admin/newplayer", component: NewplayerComponent },
  { path: "admin/newfixtures", component: NewfixturesComponent },
  { path: "admin", component: CreatenewteamComponent },
  { path: "t", component: TempDropComponent },
  { path: "**", component: SignupComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

export const routingComponents = [UsercontestsComponent, UsermatchesComponent];
