import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ContestService {
  constructor(private http: HttpClient) {}
 
  playersArray = [];
  wKArray = [];

  // Spring origin
  private springUrl = "http://localhost:8089";

  getAllContests(): Observable<any> {
    return this.http.get<any>(`${this.springUrl}/contest`, {
      responseType: "json",
    });
  }

  createContest(contest): Observable<any> {
    return this.http.post(`${this.springUrl}/addContest`, contest);
  }

  editContest(modifiedcontest): Observable<any> {
    return this.http.put(`${this.springUrl}/editcontest`, modifiedcontest);
  }

  deleteContest(index) {
    return this.http.delete(`${this.springUrl}/deletecontest/${index}`);
  }
}
