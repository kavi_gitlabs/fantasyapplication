import { TestBed } from '@angular/core/testing';

import { UsercontestService } from './usercontest.service';

describe('UsercontestService', () => {
  let service: UsercontestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UsercontestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
