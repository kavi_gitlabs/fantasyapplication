import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class UserService {
  // Spring origin
  private springUrl = "http://localhost:8080";
  allTeams: any;
  userLoginStatus: boolean = true;
  currentUser: any;
  currentUserMatchId: any;
  currentUserTeamA: any;
  currentUserTeamB: any;
  currentContest : any;

  // Points Variables
  pointsDataFinal = [];


  baPoSix: any;
  boPoDot: any;
  fiPoBoundary: any;
  fiPoSix: any; 
  fiPoCatch: any;
  oTPoCap: any;
  oTPoWK: any;
  baPoRuns: any;
  baPoBoundary: any;
  boPoWicket: any;
  fiPoRuns: any;
  oTPoRuns: any;
  appPropName: any;
  rule1: any;
  rule2: any;
  rule3: any;
  rule4: any;


  constructor(private http: HttpClient) {}

  
  


  getPointsData(): Observable<any> {
    
    return this.http.get<any>(`${this.springUrl}/matchpoint`, {
      responseType: 'text' as 'json',
    });
  }





      getAllUsers(): Observable<any> {
        return this.http.get<any>(`${this.springUrl}/users`, {
          responseType: "json",
        });
      }







  

  createUser(user): Observable<any> {
    return this.http.post(`${this.springUrl}/addUser`, user);
  }

  editUser(modifiedUser): Observable<any> {
    return this.http.put(`${this.springUrl}/editusers`, modifiedUser);
  }

  deleteUser(index) {
    return this.http.delete(`${this.springUrl}/deleteuser/${index}`);
  }

  loginUser(userObj): Observable<any> {
    return this.http.post(`${this.springUrl}/login/user`, userObj, {
      responseType: "json",
    });
  }
}
