import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FixtureService {

  constructor(private http: HttpClient) {}
  
  // Spring origin
springport :number=8087;

private springUrl = `http://localhost:${8087}`

getAllFixtures(): Observable<any> {
  return this.http.get<any>(`${this.springUrl}/fixtures`, {
    responseType: 'json',
  });
}

createFixtures(player): Observable<any> {
  return this.http.post(`${this.springUrl}/addFixture`, player);
}


editFixture(modifiedPlayer): Observable<any> {
  return this.http.put(
    `${this.springUrl}/editfixture`,
    modifiedPlayer
  ); 
}

deleteFixture(index) {
  return this.http.delete(`${this.springUrl}/deletefixture/${index}`);
}






}
 