import { Component, OnInit } from "@angular/core";
import { UsercontestService } from "src/app/usercontest.service";

@Component({
  selector: "app-usermappedcontests",
  templateUrl: "./usermappedcontests.component.html",
  styleUrls: ["./usermappedcontests.component.scss"],
})
export class UsermappedcontestsComponent implements OnInit {
  constructor(private umcService: UsercontestService) {}

  userMappedcontests: any;

  ngOnInit(): void {
    this.getUserMappedContests();
  }

  getUserMappedContests() {
    this.umcService.getAllUserContest().subscribe({
      next: (res) => {
        this.userMappedcontests = res;
        console.log(this.userMappedcontests, "Ki");
      },
    });
  }
}
