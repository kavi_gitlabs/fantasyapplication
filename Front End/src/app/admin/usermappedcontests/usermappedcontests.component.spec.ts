import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsermappedcontestsComponent } from './usermappedcontests.component';

describe('UsermappedcontestsComponent', () => {
  let component: UsermappedcontestsComponent;
  let fixture: ComponentFixture<UsermappedcontestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsermappedcontestsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UsermappedcontestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
