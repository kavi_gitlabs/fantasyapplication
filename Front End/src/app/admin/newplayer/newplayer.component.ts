import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import { Router } from "@angular/router";
import { PlayerService } from "src/app/player.service";
import { SharedService } from "src/app/shared.service";
import { Validators } from "@angular/forms";

@Component({
  selector: "app-newplayer",
  templateUrl: "./newplayer.component.html",
  styleUrls: ["./newplayer.component.scss"],
})
export class NewplayerComponent {
  playerForm: FormGroup;
  teams: any;

  constructor(
    private teamService: SharedService,
    private playerService: PlayerService,
    private fB: FormBuilder,
    private router: Router
  ) {}

  name = new FormControl("", [Validators.required, Validators.minLength(4)]);

  points = new FormControl(null, [
    Validators.required,
    Validators.pattern("^[0-9]*$"),
  ]);

  team_name = new FormControl("", [
    Validators.required,
    Validators.minLength(5),
  ]);

  skills = new FormControl("", [Validators.required]);

  ngOnInit(): void {
    this.viewTeams();

    this.playerForm = this.fB.group({
      name: this.name,
      points: this.points,
      team_name: this.team_name,
      skills: this.skills,
    });
  }

  viewTeams() {
    this.teamService.getAllTeams().subscribe({
      next: (res) => {
        this.teams = res;
      },
    });
  }

  onFormSubmit() {
    this.playerService.createPlayers(this.playerForm.value).subscribe({
      next: (res) => {
        console.log("success");

        alert("Player Added Successfully");
        this.router.navigateByUrl("admin/players");
      },
    });
  }

  cancel() {
    this.router.navigateByUrl("admin/players");
  }
}
