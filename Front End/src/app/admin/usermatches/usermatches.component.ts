import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router, TitleStrategy } from "@angular/router";
import { ContestService } from "src/app/contest.service";
import { FixtureService } from "src/app/fixture.service";
import { MapserviceService } from "src/app/mapservice.service";
import { PlayerService } from "src/app/player.service";
import { SharedService } from "src/app/shared.service";
import { UserService } from "src/app/user.service";
 
@Component({
  selector: "app-usermatches",
  templateUrl: "./usermatches.component.html",
  styleUrls: ["./usermatches.component.scss"],
})
export class UsermatchesComponent implements OnInit {
  finalCurrentUser: any;
  constructor(
    private userService: UserService,
    private fixService: FixtureService,
    private teamService: SharedService,
    public mapContestService: MapserviceService,
    public contestService: ContestService,
    public playerService: PlayerService,
    private router: Router
  ) {}
  allfixtures: any;
  allfixtureslength: number;
  allteams: any;
  objects = [];

  // loginStatus: boolean = false; 
 
  ngOnInit(): void {
    this.finalCurrentUser = this.userService.currentUser;
    this.viewFixtures();
    this.viewTeams();
    this.viewContests();
  }

  viewContests() {
    this.contestService.getAllContests().subscribe({
      next: (res) => {
        this.mapContestService.currentContests = res;
      },
    });
  } 

  avaiContests(fixtures) {
    this.userService.currentUserTeamA = fixtures.team1;
    this.userService.currentUserTeamB = fixtures.team2;


    const matchedPlayersA =
    this.playerService.allPlayers.filter(
        (obj) => obj.team_name ==   this.userService.currentUserTeamA
        
    );
    const matchedPlayersB =
    this.playerService.allPlayers.filter(
        (obj) => obj.team_name ==  this.userService.currentUserTeamB
        
    );
    const matchedPlayers = matchedPlayersA.concat(matchedPlayersB);
    this.playerService.CurrentContestPlayers = matchedPlayers;
    
    


    this.mapContestService.getAllContestByMatchID(fixtures.id).subscribe({
      next: (res) => {
        this.mapContestService.finalCurrentContests = res; 

        const matchedObjects =
          this.mapContestService.currentContests.filter((obj1) =>
            this.mapContestService.finalCurrentContests.some(
              (obj2) => obj2.contestid == obj1.id
            )
          );

          

        // console.log(matchedObjects, "Best Output");

        

        this.mapContestService.ultraFinalCurrentContests = matchedObjects;
        this.mapContestService.ultraFinalSize = matchedObjects.length;

        this.router.navigateByUrl("login/user/usercontests");
      },
    });
  }

  // Checking 

  viewFixtures() {
    this.fixService.getAllFixtures().subscribe({
      next: (res) => {

        this.allfixtures = res;
        this.allfixtureslength = this.allfixtures.length;
        // console.log(this.allfixtures);
      },
    });
  }

  
  logout() {
    // confirm()
    // this.loginStatus = false;
    // this.finalCurrentUser = undefined;
  }
  login() {
    this.router.navigateByUrl("login");
  }

  viewTeams() {
    this.teamService.getAllTeams().subscribe({
      next: (res) => {
        this.allteams = res;
        // console.log(this.allteams);
      },
    });
  }
}
