import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/shared.service';

@Component({ 
  selector: 'app-createnewteam',
  templateUrl: './createnewteam.component.html',
  styleUrls: ['./createnewteam.component.scss'],
})
export class CreatenewteamComponent {
  teamForm: FormGroup;

  constructor(
    private fB: FormBuilder,
    private sService: SharedService,
    private router: Router
  ) {}

  name = new FormControl('', [Validators.required, Validators.minLength(4)]);
  logo = new FormControl('', [Validators.required]);

  ngOnInit(): void {
    this.teamForm = this.fB.group({
      name: this.name,
      logo: this.logo,
    });
  }

  onFormSubmit() {
    this.sService.postTeam(this.teamForm.value).subscribe({
      next: (res) => {
        console.log('success');

        alert('Team Added Successfully');
        this.router.navigateByUrl('admin/allteams');
      },
    });
  }

  cancel() {
    this.router.navigateByUrl('admin/allteams');
  }
}
