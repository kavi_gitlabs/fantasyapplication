import { Component, OnInit } from '@angular/core';
import { ContestService } from 'src/app/contest.service';
import { UserService } from 'src/app/user.service';

@Component({ 
  selector: 'app-alldata',
  templateUrl: './alldata.component.html',
  styleUrls: ['./alldata.component.scss']
})
export class AlldataComponent implements OnInit {

  constructor(    private userService: UserService, private contestService: ContestService
    ) { }

  ngOnInit(): void {
    this.viewUsers(); 
    this.viewContest();
  }
  teams: any;
  teamsLength: any;
  contests: any;
  ContestLength: any;


  viewUsers() {
    this.userService.getAllUsers().subscribe({
      next: (res) => {
        // console.log(res)
        this.teams = res;
        this.teamsLength = this.teams.length;
      },
    });
  }
  viewContest(){
    this.contestService.getAllContests().subscribe({
      next: (res)=>{
        this.contests = res;
        this.ContestLength = this.contests.length;
      }
    })
  }


}
