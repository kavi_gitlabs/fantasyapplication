import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserrulesComponent } from './userrules.component';

describe('UserrulesComponent', () => {
  let component: UserrulesComponent;
  let fixture: ComponentFixture<UserrulesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserrulesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserrulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
