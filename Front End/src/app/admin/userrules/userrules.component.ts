import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user.service';

@Component({
  selector: 'app-userrules',
  templateUrl: './userrules.component.html',
  styleUrls: ['./userrules.component.scss']
})
export class UserrulesComponent implements OnInit {

  constructor(public userService: UserService) { }

  ngOnInit(): void {
  }

}
