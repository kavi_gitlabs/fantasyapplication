import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { FixtureService } from 'src/app/fixture.service';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-newfixtures',
  templateUrl: './newfixtures.component.html',
  styleUrls: ['./newfixtures.component.scss'],
})
export class NewfixturesComponent implements OnInit {
  
  constructor(
    private fixService: FixtureService,
    private fB: FormBuilder,
    private router: Router,
    private teamService: SharedService
  ) {}






  fixtureform: FormGroup;
  fixtures: any;
  teams: any;

  // Teams array  
  teamsraw = [];
  selectedTeam1 = [];
  selectedTeam2 = []; 

  team1 = new FormControl('', [Validators.required, Validators.minLength(4)]);
  team2 = new FormControl('', [Validators.required, Validators.minLength(4)]);
  datetime = new FormControl(null, [
    Validators.required,
    Validators.minLength(5),
  ]);
  venue = new FormControl('', [Validators.required]);
  stage = new FormControl('', [Validators.required]);

  ngOnInit(): void {
    this.viewFixtures();
    this.viewTeams();

    
    this.selectedTeam1 = this.teamsraw;
    this.selectedTeam2 = this.teamsraw;

    this.fixtureform = this.fB.group({
      team1: this.team1,
      team2: this.team2,
      datetime: this.datetime,
      venue: this.venue,
      stage: this.stage,
    });
  }

  clicked1() {
    this.selectedTeam2 = this.teamsraw;
    this.selectedTeam2 = this.selectedTeam2.filter(
      (team) => team !== this.fixtureform.value.team1
    );
   
  }
 
  
  clicked2() {
    this.selectedTeam1 = this.teamsraw;
    this.selectedTeam1 = this.selectedTeam1.filter(
      (team) => team !== this.fixtureform.value.team2
      );
  }


  viewTeams() {
    this.teamService.getAllTeams().subscribe({
      next: (res) => {
        this.teams = res;
        this.teams.forEach((val) => this.teamsraw.push(val.name));
      },
    });
  }

  viewFixtures() {
    this.fixService.getAllFixtures().subscribe({
      next: (res) => {
        this.fixtures = res;
      },
    });
  }

  onFormSubmit() {
    this.fixService.createFixtures(this.fixtureform.value).subscribe({
      next: (res) => {
        console.log('success');
        console.log(this.fixtureform.value.datetime, 'Date time from value');

        alert('New Match Added Successfully');
        this.router.navigateByUrl('admin/fixtures');
      },
    });
  }

  cancel() {
    this.router.navigateByUrl('admin/fixtures');
  }
}
