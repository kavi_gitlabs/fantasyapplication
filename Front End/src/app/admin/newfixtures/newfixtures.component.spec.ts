import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewfixturesComponent } from './newfixtures.component';

describe('NewfixturesComponent', () => {
  let component: NewfixturesComponent;
  let fixture: ComponentFixture<NewfixturesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewfixturesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewfixturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
