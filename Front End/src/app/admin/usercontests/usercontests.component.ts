import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ContestService } from "src/app/contest.service";
import { MapserviceService } from "src/app/mapservice.service";
import { UserService } from "src/app/user.service";

@Component({
  selector: "app-usercontests",
  templateUrl: "./usercontests.component.html",  
  styleUrls: ["./usercontests.component.scss"],
})
export class UsercontestsComponent implements OnInit {
  constructor(
    public mapContestService: MapserviceService,
    public userService: UserService,
    private router: Router

  ) {}

  ngOnInit(): void {
  }

  join(availableContest) {
    console.log(availableContest,"Lock and load");
    this.userService.currentContest =  availableContest;
    console.log(this.userService.currentContest,"Lo");
    
    
    this.router.navigateByUrl("login/user/userplayers");
  }

}
 