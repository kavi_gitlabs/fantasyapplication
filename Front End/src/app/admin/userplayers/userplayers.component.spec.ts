import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserplayersComponent } from './userplayers.component';

describe('UserplayersComponent', () => {
  let component: UserplayersComponent;
  let fixture: ComponentFixture<UserplayersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserplayersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserplayersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
