import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WkeeperComponent } from './wkeeper.component';

describe('WkeeperComponent', () => {
  let component: WkeeperComponent;
  let fixture: ComponentFixture<WkeeperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WkeeperComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WkeeperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
