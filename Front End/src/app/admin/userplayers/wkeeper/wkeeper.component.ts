import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ContestService } from "src/app/contest.service";
import { PlayerService } from "src/app/player.service";
import { UserService } from "src/app/user.service";
import { UsercontestService } from "src/app/usercontest.service";

@Component({
  selector: "app-wkeeper",
  templateUrl: "./wkeeper.component.html",
  styleUrls: ["./wkeeper.component.scss"],
})
export class WkeeperComponent implements OnInit {
  PlayerArray = [];
  PlayerArrayWK = [];
  PlayerArrayWKStatus = false;
  PlayerArrayLengthStatus = false;
  PlayerPointsStatus = false;
  AllStatusFalse = false;
  AllStatusTick = false;
  TeamArrayMaxLengthStatus = false;
  sum = 0;

  playersfixArray = [];

  editStatus: boolean;
  editProductIndex: number;
  chooseForm: FormGroup;
  usercontestForm: FormGroup;

  CurrentContestPlayerslength = 0;
  
  isCaptain = new FormControl('', [Validators.required]);
  isWicketKeeper = new FormControl('', [Validators.required]);

  constructor(
    public playerService: PlayerService,
    public conService: ContestService,
    public userService: UserService,
    private fB: FormBuilder,
    private UserContService: UsercontestService,
    private router: Router,

  ) {}

  ngOnInit(): void {


    this.chooseForm =this.fB.group({
      isWicketKeeper : this.isWicketKeeper,
      isCaptain : this.isCaptain,
    })

    this.CurrentContestPlayerslength =
      this.playerService.CurrentContestPlayers?.length;

    // this.usercontestForm = this.fB.group({
    //   contestname:"",
    //   contestid: "",
    //   username:"",
    //   userid: "",
    //   matchid: "",
    //   userPlayerData: [],
    // });

    this.getAllUserMappedContestData();
  }

  getAllUserMappedContestData() {
    this.UserContService.getAllUserContest().subscribe({
      next: (res) => {
      },
    });
  }

  edit(user: any) {
    this.PlayerArray?.push(user);

    this.conService.playersArray = [...new Set(this.PlayerArray)];

    this.new();
    this.check();
  }
  onFormSubmit(){
    console.log(this.chooseForm.value,"Follow up")
  }

  new() {
    this.sum = this.conService.playersArray.reduce(
      (accumulator, currentValue) => {
        return accumulator + currentValue.points;
      },
      0
    );
  }
  remove(user) {
    this.PlayerArray = this.PlayerArray?.filter((item) => item.id !== user.id);
    this.conService.playersArray = [...new Set(this.PlayerArray)];

    this.new();
    this.check();
    this.wicketKeeperPick();
  }

  wicketKeeperPick() {
    const foundObject = this.conService.playersArray.filter(
      (obj) => obj.skills === "Wicket Keeper"
    );

    this.conService.wKArray = [...new Set(foundObject)];

  }

  check() {
    // Wicket keeper checking = Working
    const foundObject = this.conService.playersArray.find(
      (obj) => obj.skills === "Wicket Keeper"
    );

    if (foundObject) {
      this.PlayerArrayWKStatus = false;
    } else {
      this.PlayerArrayWKStatus = true;
    }

    // Seven Player Condition

    const teamArrayOf_A = this.conService.playersArray.filter(
      (obj) => obj.team_name === this.userService.currentUserTeamA
    );
    const teamArrayOf_B = this.conService.playersArray.filter(
      (obj) => obj.team_name === this.userService.currentUserTeamB
    );

    if (teamArrayOf_A.length > 7 || teamArrayOf_B.length > 7) {
      this.TeamArrayMaxLengthStatus = true;
    } else {
      this.TeamArrayMaxLengthStatus = false;
    }

    // Player length checking = working

    const playerArrayLength = this.conService.playersArray.length;
    if (playerArrayLength == 10) {
      this.PlayerArrayLengthStatus = false;
    } else {
      this.PlayerArrayLengthStatus = true;
    }

    // Player Total points = working

    if (this.sum <= 100) {
      this.PlayerPointsStatus = false;
    } else {
      this.PlayerPointsStatus = true;
    }

    if (
      !this.PlayerPointsStatus &&
      !this.PlayerArrayLengthStatus &&
      !this.PlayerArrayWKStatus &&
      !this.TeamArrayMaxLengthStatus
    ) {

      this.AllStatusTick = true;
      this.AllStatusFalse = false;
    } else {
      this.AllStatusTick = false;
      this.AllStatusFalse = true;
    }
    this.wicketKeeperPick();
  }

  saveUserContest() {
    if (
      !this.PlayerPointsStatus &&
      !this.PlayerArrayLengthStatus &&
      !this.PlayerArrayWKStatus &&
      !this.TeamArrayMaxLengthStatus
    ) {
      this.usercontestForm = this.fB.group({
        contestid: this.userService.currentContest.id,
        userid: this.userService.currentUser.id,
        matchid: this.userService.currentUser.id,
        isCaptain : this.chooseForm.value.isCaptain,
        isWicketKeeper : this.chooseForm.value.isWicketKeeper
      });

      this.usercontestForm.value.userPlayerData = [
        ...new Set(this.PlayerArray),
      ];

      this.UserContService.createUserContest(
        this.usercontestForm.value
      ).subscribe({
        next: (res) => {
          console.log(this.usercontestForm.value,"get the success");
          
          console.log("success");

          alert("User Mapped a team to a contest");
          this.router.navigateByUrl('/login/user');
        },
      });
    }
  }
}
