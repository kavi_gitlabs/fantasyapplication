import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PlayerService } from "src/app/player.service";
import { UserService } from "src/app/user.service";

@Component({
  selector: "app-userplayers",
  templateUrl: "./userplayers.component.html",
  styleUrls: ["./userplayers.component.scss"],
})
export class UserplayersComponent implements OnInit {
  constructor(
    public userService: UserService,
    private router: Router,
    public playerService: PlayerService
  ) {}
 
  ngOnInit(): void {
    this.wk()
  }

  wk() {
    const matchedSkilledPlayers = this.playerService.CurrentContestPlayers?.filter(
      (obj) => obj.skills == "Wicket Keeper"
    );
    this.playerService.SkilledPlayers = matchedSkilledPlayers;
  }

  batter() {
    const matchedSkilledPlayers = this.playerService.CurrentContestPlayers.filter(
      (obj) => obj.skills == "Batter"
    );
    this.playerService.SkilledPlayers = matchedSkilledPlayers;

  }

  bowler() {
    const matchedSkilledPlayers = this.playerService.CurrentContestPlayers.filter(
      (obj) => obj.skills == "Bowler"
    );
    this.playerService.SkilledPlayers = matchedSkilledPlayers;

  }


  allrounder() {
    const matchedSkilledPlayers = this.playerService.CurrentContestPlayers.filter(
      (obj) => obj.skills == "All Rounder"
    );
    this.playerService.SkilledPlayers = matchedSkilledPlayers;

  }

}
