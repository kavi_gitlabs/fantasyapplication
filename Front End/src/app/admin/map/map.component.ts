import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { ContestService } from "src/app/contest.service";
import { FixtureService } from "src/app/fixture.service";
import { MapserviceService } from "src/app/mapservice.service";
import { FixturesComponent } from "../fixtures/fixtures.component";

@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.scss"],
})
export class MapComponent implements OnInit {
  constructor(
    private mapService: MapserviceService,
    private fixService: FixtureService, 
    private fB: FormBuilder,
    private conService: ContestService
  ) {}

  ngOnInit(): void {
    this.viewMappedContests();
    this.viewFixtures();
    this.viewContests(); 
    this.mapform = this.fB.group({
      contestdata: this.contestdata,
      fixturedata: this.fixturedata,
    });
  }

  contestdata = new FormControl("", [Validators.required]);
  fixturedata = new FormControl("", [Validators.required]);

  mappedContest: any;
  id: string;
  allFixtures: any;
  allContests: any;

  mapform: FormGroup;

  // Property 'mapform' has no initializer and is not definitely assigned in the constructor.ts(2564)

  mapmainform: FormGroup;

  selectedValue: any;
  selectedValueContest: any;
  mapNameStatus: Boolean = false;

  fixValue: any;

  fixdata() {
    // console.log(this.mappedContest);
    let fixId = this.fixturedata.value.split("-")[2];
    let conId = this.contestdata.value.split("-")[1];
    this.mapNameStatus = false;

    // if (fixId === undefined || conId == undefined) {
    //   this.mapNameStatus = false;
    // } else
     if (fixId && conId) {
      // console.log("working fine");
      // console.log("map", this.mappedContest, "map");

      this.mappedContest.find((element) => {
        if (element.contestid == conId && element.matchid == fixId) {
          // console.log("ok");
          this.mapNameStatus = true;
          
        }
      })
      ;
    }
  }

  onFormSubmit() {
    this.mapform = this.fB.group({
      contestdata: this.contestdata,
      fixturedata: this.fixturedata,
    });


    let myArrayfixture = this.fixturedata.value.split("-");
    let myArraycontest = this.contestdata.value.split("-");

    this.mapmainform = this.fB.group({
      team1: myArrayfixture[0],
      team2: myArrayfixture[1],
      matchid: myArrayfixture[2],
      contestname: myArraycontest[0],
      contestid: myArraycontest[1],
    });

    this.mapService.createMapContest(this.mapmainform.value).subscribe({
      next: (res) => {
        if (res === 0) {
          this.mapNameStatus = true;
        } else {
          this.mapNameStatus = false;
          alert("Fixture and Match mapped Successfully");
          this.viewMappedContests();
        }
      },
    });
    this.fixdata()
  }

  // CRUD operation

  viewFixtures() {
    this.fixService.getAllFixtures().subscribe({
      next: (res) => {
        this.allFixtures = res;
        // console.log(res, "Hit from fixtures contest");
      },
    });
  }

  viewContests() {
    this.conService.getAllContests().subscribe({
      next: (res) => {
        this.allContests = res;
        // console.log(res, "Hit from Contests ");
      },
    });
  }

  viewMappedContests() {
    this.mapService.getAllMappedContest().subscribe({
      next: (res) => {
        // console.log(res, "Hit from map contest");
        this.mappedContest = res;
      },
    });
  }

  delete(user: any) {
    this.id = user.id;
    if (
      confirm(`Are you sure you want to delete Player Data : ${user.name} ?`)
    ) {
      this.mapService.deletemappedContest(this.id).subscribe({
        next: (res) => {
          this.viewMappedContests();
        },
      });
    }
  }
}
