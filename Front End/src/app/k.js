 
// // ```javascript
// // Your original array
// let originalArray = [{ id: 1, name: 'John' }, { id: 2, name: 'Jane' }];
 
// // Array to add or update
// let newArray = [{ id: 1, name: 'Alice' }, { id: 3, name: 'Bob' }];
 
// // Combine arrays and replace existing values based on 'id'
// let combinedArray = [...originalArray, ...newArray].reduce((acc, curr) => {
// const existingIndex = acc.findIndex(item => item.id === curr.id);
 
//     if (existingIndex !== -1) {
//         // Replace existing item
//         acc[existingIndex] = curr;
//     } else {
//         // Add new item
//         acc.push(curr);
//     }
 
//     return acc;
// }, []);
 
// console.log(combinedArray);
// // ```












// // ```javascript
// // Your original array
// let originalArray = [{ id: 1, name: 'John' }, { id: 2, name: 'Jane' }];
 
// // Array to add or update
// let newArray = [{ id: 1, name: 'Alice' }, { id: 3, name: 'Bob' }];
 
// // Combine arrays and replace existing values based on 'id' using Map
// let idMap = new Map(originalArray.map(obj => [obj.id, obj]));
 
// newArray.forEach(obj => idMap.set(obj.id, obj));
 
// let combinedArray = Array.from(idMap.values());
 
// console.log(combinedArray);
// // ```








// ```javascript
// Your original array
let originalArray = [{ id: 1, name: 'John' }, { id: 2, name: 'Jane' }];
 
// Array to add or update
let newArray = [{ id: 1, name: 'Alice' }, { id: 3, name: 'Bob' }];
 
// Update original array based on 'id'
newArray.forEach(newObj => {
let index = originalArray.findIndex(origObj => origObj.id === newObj.id);
 
    if (index !== -1) {
        // Replace existing object
        originalArray[index] = newObj;
    } else {
        // Add new object
        originalArray.push(newObj);
    }
});
 
console.log(originalArray);
// ```
 









 