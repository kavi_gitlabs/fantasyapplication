import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SharedService } from './shared.service';
import { UserService } from './user.service';
import { PlayerService } from './player.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'New App';

  allTeams: any;
  LoginStatusFetched: Boolean = true;

  constructor(private http: HttpClient, private userService: UserService,public playerService: PlayerService) {}

  ngOnInit() {
    // this.LoginStatusFetched = this.userService.userLoginStatus;
    this.viewPlayers();

  }
  viewPlayers() {
    this.playerService.getAllPlayers().subscribe({
      next: (res) => {
        this.playerService.allPlayers = res;
      },
    });
  }
}
