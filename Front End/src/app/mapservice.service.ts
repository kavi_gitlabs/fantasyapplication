import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class MapserviceService {
  // Spring origin port 

  private springUrl = "http://localhost:8091";
  currentContests: any;
  finalCurrentContests: any;
  ultraFinalCurrentContests: any;
  ultraFinalSize: any;

  constructor(private http: HttpClient) {}

  getAllMappedContest(): Observable<any> {
    return this.http.get<any>(`${this.springUrl}/mappedcontests`, {
      responseType: "json",
    });
  }

  createMapContest(player): Observable<any> {
    return this.http.post(`${this.springUrl}/mapcontest`, player);
  }

  deletemappedContest(index) {
    return this.http.delete(`${this.springUrl}/deletemappedcontest/${index}`);
  }

  getAllContestByMatchID(index): Observable<any> {
    console.log(index, "Form");
    return this.http.get<any>(`${this.springUrl}/mappedcontests/${index}`, {
      responseType: "json",
    });
  }
}
