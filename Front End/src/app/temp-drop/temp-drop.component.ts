import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-temp-drop',
  templateUrl: './temp-drop.component.html',
  styleUrls: ['./temp-drop.component.scss'],
})
export class TempDropComponent implements OnInit {
  constructor(private fb: FormBuilder) {}




  
  cars = ['Saab', 'Volvo', 'BMW'];
  cars1 = [];
  cars2 = [];
  editTable: FormGroup;

  ngOnInit(): void {
    this.editTable = new FormGroup({
      carname1: new FormControl(''),
      carname2: new FormControl(''),
    });
    this.cars2 = this.cars;
    this.clicked1();
    this.clicked2();
  }

  // __________________
  clicked1() {
    this.cars2 = this.cars;
    this.cars2 = this.cars2.filter((v) => v !== this.editTable.value.carname1);
  }
  clicked2() {
    this.cars1 = this.cars;
    this.cars1 = this.cars1.filter((v) => v !== this.editTable.value.carname2);
  }
}
