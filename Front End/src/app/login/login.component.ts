import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

  
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})

export class LoginComponent implements OnInit {
  userForm: FormGroup | undefined;
  errMessage: string;
  errStatus = false; 

  constructor(
    private fB: FormBuilder,
    private router: Router, 
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.userForm = this.fB.group({
      username: 'kavi',
      password: 'kavi',
    });
  }

  login(){
    this.router.navigateByUrl("/signup")
  }
  onFormSubmit() { 
  
    let userObj = this.userForm.value;

    this.userService.loginUser(userObj).subscribe({
      next: (res) => { 
        // console.log(typeof res.id);
        // console.log(res);

        if (res.id  == 0) {
          this.errStatus=true 
        } 
        
        else {

          this.errStatus=false;
          this.userService.currentUser = res;
          this.userService.userLoginStatus = false;
          // console.log(this.userService.currentUser);
          this.router.navigateByUrl('/login/user');
        }
      },
      error: (err) => {
        console.log('user login error', err);
      },
    })
  }
}
