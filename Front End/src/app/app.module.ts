import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MatchesComponent } from './matches/matches.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { MyteamComponent } from './myteam/myteam.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminComponent } from './admin/admin.component';
import { CreatenewteamComponent } from './admin/createnewteam/createnewteam.component';
import { AllusersComponent } from './admin/allusers/allusers.component';
import { CreatecontestComponent } from './admin/createcontest/createcontest.component';
import { AllTeamsComponent } from './admin/all-teams/all-teams.component';
import { UserComponent } from './user/user.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { PlayerComponent } from './admin/player/player.component';
import { NewplayerComponent } from './admin/newplayer/newplayer.component';
import { FixturesComponent } from './admin/fixtures/fixtures.component';
import { ResultComponent } from './admin/result/result.component';
import { ChangepasswordComponent } from './admin/changepassword/changepassword.component';
import { RulesComponent } from './admin/rules/rules.component';
import { LivematchComponent } from './admin/livematch/livematch.component';
import { AlldataComponent } from './admin/alldata/alldata.component';
import { NewfixturesComponent } from './admin/newfixtures/newfixtures.component';
import { TempDropComponent } from './temp-drop/temp-drop.component';
import { FilterPipe } from './filter.pipe';
import { ContestComponent } from './admin/contest/contest.component';
import { MapComponent } from './admin/map/map.component';
import { UsermatchesComponent } from './admin/usermatches/usermatches.component';
import { UsercontestsComponent } from './admin/usercontests/usercontests.component';
import { UserrulesComponent } from './admin/userrules/userrules.component';
import { UserplayersComponent } from './admin/userplayers/userplayers.component';
import { WkeeperComponent } from './admin/userplayers/wkeeper/wkeeper.component';
import { BatterComponent } from './admin/userplayers/batter/batter.component';
import { BowlerComponent } from './admin/userplayers/bowler/bowler.component';
import { AllrounderComponent } from './admin/userplayers/allrounder/allrounder.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { UsermappedcontestsComponent } from './admin/usermappedcontests/usermappedcontests.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MatchesComponent,
    LoginComponent,
    SignupComponent,
    MyteamComponent,
    AdminComponent,
    CreatenewteamComponent,
    AllusersComponent,
    CreatecontestComponent,
    AllTeamsComponent,
    UserComponent,
    AdminLoginComponent,
    PlayerComponent,
    NewplayerComponent,
    FixturesComponent,
    ResultComponent,
    ChangepasswordComponent,
    RulesComponent,
    LivematchComponent,
    AlldataComponent,
    NewfixturesComponent,
    TempDropComponent,
    FilterPipe,
    ContestComponent,
    MapComponent,
    UsermatchesComponent,
    UsercontestsComponent,
    UserrulesComponent,
    UserplayersComponent,
    WkeeperComponent,
    BatterComponent,
    BowlerComponent,
    AllrounderComponent,
    UsermappedcontestsComponent  ], 
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
