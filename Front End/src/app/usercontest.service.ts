import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsercontestService {

  private springUrl = "http://localhost:8093";

  constructor(private http: HttpClient) {}

  getAllUserContest(): Observable<any> {
    return this.http.get<any>(`${this.springUrl}/mappedUserContest`, {
      responseType: "json",
    });
  }

// lllll

  createUserContest(usercontestdata): Observable<any> {
    return this.http.post(`${this.springUrl}/createMappedUserContest`, usercontestdata);
  }


}
