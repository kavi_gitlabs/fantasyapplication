import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
}) 
export class SignupComponent { 
  userForm: FormGroup | undefined;
  userNameStatus: Boolean = false;
  constructor(
    private fB: FormBuilder,
    private http: HttpClient,
    private uService: UserService,
    private router: Router 
  ) {}

  firstname = new FormControl('', [Validators.required, Validators.minLength(4)]);
  lastname= new FormControl('', [Validators.required, Validators.minLength(4)]);
  username= new FormControl('', [Validators.required, Validators.minLength(4)]);
  password= new FormControl('', [Validators.required, Validators.minLength(4)]);
  email= new FormControl('', [Validators.required, Validators.minLength(4)]);
  mobile= new FormControl('', [Validators.required, Validators.minLength(4)]);
  ngOnInit(): void {
    this.userForm = this.fB.group({
      firstname:this.firstname,
      lastname:this.lastname,
      username:this.username,
      password:this.password,
      email: this.email,
      mobile: this.mobile,
    });
  }
 
  
  onFormSubmit() { 

    console.log(this.userForm.value)
    this.uService.createUser(this.userForm.value).subscribe({
      next: (res) => {

        if(res===0){
          this.userNameStatus=true;
        }
        
        else{
          console.log('success');
  
        }

      },
    });
  }

  login(){
    this.router.navigateByUrl("/login")
  }

}
